﻿<?php

$_['heading_title']				= 'So Обратный отсчет всплывающих окон';
$_['heading_title_so']			= 'So Обратный отсчет всплывающих окон <b style="размер шрифта:10px; фон:#aaa; цвет:#fff; отступы: 3px;толщина шрифта:normal">версия 1.1.0</b>';

$_['text_list']					= 'Список всплывающих окон';
$_['text_edit']					= 'Редактировать модуль';
$_['text_add']					= 'Добавить';
$_['text_status']				= 'Статус';
$_['text_status_help']			= 'Модуль Включен/Отключен';
$_['text_name']					= 'Название';
$_['text_name_help']			= 'Название модуля';
$_['entry_name']				= 'Название модуля';
$_['text_priority']				= 'Приоритет';
$_['text_priority_help']		= 'Порядок сортировки';
$_['entry_priority']			= 'Приоритет';
$_['text_width']				= 'Ширина (px)';
$_['text_width_help']			= 'Ширина всплывашки';
$_['entry_width']				= 'Ширина всплывашки (px)';
$_['text_height']				= 'Высота (px)';
$_['text_height_help']			= 'Высота всплывашки';
$_['entry_height']				= 'Высота всплывашки (px)';
$_['text_opacity']				= 'Прозрачность (0->1)';
$_['text_opacity_help']			= 'Прозрачность';
$_['entry_opacity']				= 'Прозрачность';
$_['text_display_countdown']	= 'Отображать обратный отсчет';
$_['text_date_start']			= 'Время старта';
$_['text_date_start_help']		= 'Таймер отображения с даты начала и до конца истекает';
$_['entry_date_start']			= 'Дата начала';
$_['text_date_expire']			= 'Дата конца';
$_['text_date_expire_help']		= 'Вывод таймера с даты начала до даты конца';
$_['entry_date_expire']			= 'Дата истечения срока действия';
$_['text_heading_title']		= 'Заголовок';
$_['text_heading_title_help']	= 'Выводить заголовок';
$_['text_content']				= 'Описание';
$_['text_content_help']			= 'Выводить контент в всплывашке';
$_['text_image']				= 'Банер обратного отсчета';
$_['text_image_help']			= 'Отображать банер';
$_['text_link']					= 'Ссылка банера';
$_['entry_store']				= 'Магазин';
$_['text_module']				= 'Модуль';
$_['text_success']				= 'Готово: Вы изменили модуль So Popup Countdown!';
$_['error_permission']			= 'Внимание: У вас нет разрешения на модификацию модуля!';
$_['error_warning']				= 'Внимание: Вы должны тщательно проверить модуль';
$_['error_name']				= 'Имя модуля должно быть от 3 до 64 символов!';
$_['error_width']				= 'Width is not empty and is numeric';
$_['error_height']				= 'Height is not empty and is numeric';
$_['error_date']				= 'Дата истечения срока действия должна быть больше даты начала';

$_['tab_module_setting']		= 'Настройки модуля';
$_['tab_help']					= 'Помощь';

$_['column_image']				= 'Картинка';
$_['column_name']				= 'Название';
$_['column_sort_order']			= 'Приоритет';
$_['column_status']				= 'Статус';
$_['column_date_start_expire']	= 'Дата старта / Дата окончания';
$_['column_store']				= 'Магазин';
$_['column_action']				= 'Действие';

$_['button_savestay']			= 'Сохранить & Остаться';