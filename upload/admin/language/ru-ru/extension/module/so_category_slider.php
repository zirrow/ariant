﻿<?php
// Heading
$_['heading_title']             = 'So Слайдер категорий';
$_['heading_title_so']          = 'So Слайдер категорий <b style="размер шрифта:10px; фон:#aaa; цвет:#fff; отступы: 3px;толщина шрифта:normal">версия 2.2.0</b>';
$_['entry_button_clear_cache']  = 'Сбросить кешь';
// Text
$_['text_module']      			= 'Модули';
$_['text_success']     			= 'Успех: Вы изменили настройки!';
$_['text_success_remove']       = 'Успех: Сброс SO кеша произведен успешно!';
$_['text_edit']       		 	= 'Редактирование модуля So Слайдер категорий';
$_['text_create_new_module']	= 'Создать новый модуль';
$_['text_edit_module'] 			= 'Редактирвоать модуль ';
$_['text_layout']               = 'После того как вы добавили модуль и настроили его вы можете добавить его в шаблон <a href="%s" class="alert-link">здесь</a>!';
$_['text_show']                 = 'Показать';
$_['text_hide']                 = 'Скрыть';
// value
$_['value_blank']			    = 'Новое окно';
$_['value_self']			    = 'То же окно';
$_['value_name']       		    = 'Имя';
$_['value_model']       	    = 'Модель';
$_['value_price']       	    = 'Цена';
$_['value_quantity']        	= 'Количество';
$_['value_rating']       	    = 'Рейтинг';
$_['value_sort_order']          = 'Порядок сортировки';
$_['value_date_added']          = 'Дота добавления';
$_['value_sales']       	    = 'Распродажа';
$_['value_asc']       		    = 'По возростанию';
$_['value_desc']       		    = 'По убыванию';
$_['value_top']       		    = 'Сверху';
$_['value_under']       	    = 'Снизу';
//Default
$_['entry_include']             = 'Добавить';
$_['entry_exclude']             = 'Исключить';

// button
$_['entry_button_save']       		= 'Сохранить';
$_['entry_button_save_and_edit']    = 'Сохранить и редактировать';
$_['entry_button_save_and_new']    	= 'Сохранить и создать новый';
$_['entry_button_cancel']       	= 'Отменить';
$_['button_add_module'] 			= 'Добавить модуль';
$_['entry_button_delete']       	= 'Удалить';

// Entry
$_['entry_name']              			= 'Название модуля';
$_['entry_name_desc']         			= 'Модуль должен иметь название';
$_['entry_head_name']        			= 'Заголовок';
$_['entry_head_name_desc']    			= 'Заголовок';
$_['entry_display_title_module']       	= 'Отображать заголовок';
$_['entry_display_title_module_desc']   = 'Отображать заголовок модуля.';
$_['entry_status']     					= 'Статус';
$_['entry_status_desc']     			= 'Опубликовать/Спрятать модуль';

$_['entry_module']            	= 'Общие настройки';
$_['entry_source_option']     	= 'Нстройки источника';
$_['entry_tabs_option']       	= 'Параметры вкладок';
$_['entry_category_option']   	= 'Нстройки категорий';
$_['entry_product_option']  	= 'Настройки продуктов';
$_['entry_image_option']      	= 'Нстройки картинок';
$_['entry_effect_option']     	= 'Настройка эффектов';
$_['entry_advanced_option']     = 'Дополнительные настрйоки';

//Tabs General
$_['entry_open_link']  			= 'Открыть ссылку';
$_['entry_open_link_desc']  	= 'Тип отображается, когда вы нажимаете на ссылку';
$_['entry_class_suffix']     	= 'Class Suffix';
$_['entry_class_suffix_desc']   = 'Class Suffix';
$_['entry_column']     			= '# колотки';
$_['entry_nb_column0_desc']     = 'Для устроуств с шириной экрана >= 1200px и больше.';
$_['entry_nb_column1_desc']     = 'Для устроуств с шириной экрана от >= 992px до < 1200px.';
$_['entry_nb_column2_desc']     = 'Для устроуств с шириной экрана от >= 768px до < 992px.';
$_['entry_nb_column3_desc']     = 'Для устроуств с шириной экрана от >= 480px до < 768px ';
$_['entry_nb_column4_desc']     = 'Для устроуств с шириной экрана от < 480px ';

//Tabs Source options
$_['entry_category']              	= 'Категории';
$_['entry_category_desc']         	= 'Выбор категорий';
$_['entry_child_category']        	= 'Продукты дочерней категории';
$_['entry_child_category_desc']  	= 'Добавить или убрать продукты из дочерних категорий.';
$_['entry_category_depth']        	= 'Глубина категории продуктов';
$_['entry_category_depth_desc']  	= 'Количество возвращаемых продуктов дочерних категорий.';
$_['entry_product_order']   		= 'Сортировка продукта по';
$_['entry_product_order_desc']		= 'Сортировка продукта';
$_['entry_ordering']     			= 'Направление сортировки';
$_['entry_ordering_desc']   		= 'Выберите направление, по которому вы хотите сортировать товары.';

$_['entry_source_limit']          	= 'Количество';
$_['entry_source_limit_desc'] 		= 'Количество товаров для отображения. Значение по умолчанию - 0, будут отображаться все продукты.';

$_['entry_display_feature']          	= 'Отображать Product Feature';
$_['entry_display_feature_desc'] 		= 'Отображение Product Feature.';

$_['entry_product_feature']          	= 'Product Feature';
$_['entry_product_feature_desc'] 		= 'Product Feature.';

// Products Options
$_['entry_display_title']   			= 'Отображать название';
$_['entry_display_title_desc']   		= 'Показать название товара';
$_['entry_title_maxlength']     		= 'Макс. длина названия';
$_['entry_title_maxlength_desc']     	= 'Максимальная длина заголовка по символам. Введите значение 0, если вы хотите показать все. Введите целое число >= 0.';
$_['entry_display_description']     	= 'Отобразить описание';
$_['entry_display_description_desc']    = 'Отобразить описание для продуктов';
$_['entry_description_maxlength']     	= 'Макс. длина описания';
$_['entry_description_maxlength_desc']  = 'Максимальная длина описания по символам. Введите значение 0, если вы хотите показать все. Введите целое число >= 0.';
$_['entry_product_image']         		= 'Показать изображение продукта';
$_['entry_product_image_desc']     		= 'Категория Полный текст';
$_['entry_product_image_num']         	= 'Номер изображения проудкта';
$_['entry_product_image_num_desc']     	= 'Показать номер изображения для продукта';
$_['entry_width']                 		= 'Ширина';
$_['entry_width_desc'] 					= 'Ширина картинки';
$_['entry_height']                		= 'Высота';
$_['entry_height_desc'] 				= 'Высота картинки';
$_['entry_nb_row']                  	= '# рядов';
$_['entry_nb_row_desc']             	= 'Число строк ';
$_['entry_background']            		= 'Background';
$_['entry_background_desc']            	= 'Подложка';
$_['entry_display_rating']     			= 'Отображать рейтинг';
$_['entry_display_rating_desc']     	= 'Включить/отключить рейтинг продуктов.';
$_['entry_display_price']     			= 'Отображать цену';
$_['entry_display_price_desc']     		= 'Включить/отключить цену продуктов.';
$_['entry_products_style']  		    = 'Выберите стиль';
$_['entry_products_style_desc']         = 'Стильные представления продуктов';
$_['entry_display_readmore_link']     	= 'Показать ссылку "подробнее"';
$_['entry_display_readmore_link_desc']  = 'Включение или отключение ссылки "подробнее"';
$_['entry_readmore_text']     	        = 'Текст для ктонки "подробнее"';
$_['entry_readmore_text_desc']          = 'Текст для ктонки "подробнее"';
$_['entry_display_add_to_cart']     	= 'Отображать "В корзину"';
$_['entry_display_add_to_cart_desc']    = 'Отображать кнопку "В корзину"';
$_['entry_display_wishlist']     	    = 'Отобразить "Список желаевых"';
$_['entry_display_wishlist_desc']       = 'Отобразить кнопку "Список желаевых"';
$_['entry_display_compare']     	    = 'Отображать "В сравнения"';
$_['entry_display_compare_desc']        = 'Отображать кнопку "В сравнения"';
$_['entry_display_sale']     	    	= 'Отображать Акции';
$_['entry_display_sale_desc']        	= 'Отображать Акции';
$_['entry_display_new']     	    	= 'Отображать Новинки';
$_['entry_display_new_desc']        	= 'Отображать Новинки';
$_['entry_date_day']     	    		= 'Количество дней для новинок';
$_['entry_date_day_desc']        		= 'Количество дней которое товар будет считаться новым';

//Tabs Category options
$_['entry_category_label'] 					= 'Опции категорий:';
$_['entry_sub_category_label'] 				= 'Опции под категорий:';
$_['entry_cat_title_display']         		= 'Отображать название категорий';
$_['entry_cat_title_display_desc']      	= 'Отображать название категорий';
$_['entry_cat_image_display']         		= 'Отобразить картинку категории';
$_['entry_cat_image_display_desc']      	= 'Отобразить картинку категории';
$_['entry_cat_title_maxcharacs']       		= 'Максимальная длина названия категории';
$_['entry_cat_title_maxcharacs_desc']      	= 'Максимальная днима названия категории. 0 для отклчения лимита!';

$_['entry_child_category_cat']        	    = 'Под категории';
$_['entry_child_category_cat_desc']  	    = 'Добавить или убрать подкатегории.';
$_['entry_cat_sub_title_maxcharacs'] 		= 'Максимальная длина названия подкатегории';
$_['entry_cat_sub_title_maxcharacs_desc'] 	= 'Максимальная днима названия подкатегории. 0 для отклчения лимита!';
$_['entry_cat_all_product'] 				= 'Отображать "всего товаров"';
$_['entry_cat_all_product_desc'] 			= 'Отображать общее количество товаров';
$_['entry_category_width']                 	= 'Ширина категории';
$_['entry_category_width_desc'] 			= 'Category Width of image';
$_['entry_category_height']                	= 'Высота категории';
$_['entry_category_height_desc'] 			= 'Category Height of image';
$_['entry_placeholder_path']      		    = 'Путь к Placeholder (или Url)';
$_['entry_placeholder_path_desc']      	    = 'Путь (или URL) к картинке по умолчанию';
$_['entry_source_limit_cat']          	    = 'Количество категорий';
$_['entry_source_limit_cat_desc'] 		    = 'Число отображаемых категорий. Значение по умолчанию 0 будет отображать всю категорию.';

//Tabs Effect options
$_['effect_none'] 						= 'Никаких';
$_['effect_fadeIn'] 					= 'Fade In';
$_['effect_zoomIn'] 					= 'Приблизить';
$_['effect_zoomOut'] 					= 'Отдалить';
$_['effect_slideLeft'] 					= 'Перелистывание влево';
$_['effect_slideRight'] 				= 'Перелистывание вправа';
$_['effect_slideTop'] 					= 'Перелистывание вверх';
$_['effect_slideBottom'] 				= 'Перелистывание вних';
$_['effect_flip'] 						= 'Flip';
$_['effect_flipInX'] 					= 'Flip in Horizontal';
$_['effect_flipInY'] 					= 'Flip in Vertical';
$_['effect_starwars'] 					= 'Star war';
$_['effect_bounceIn'] 					= 'Bounce In';
$_['effect_bounceInUp'] 				= 'Bounce In Up';
$_['effect_bounceInDown'] 				= 'Bounce In Down';
$_['effect_pageTop'] 					= 'Page Top';
$_['effect_pageBottom'] 				= 'Flip';

$_['entry_margin']                		= 'Отступ в право';
$_['entry_margin_desc']             	= 'Margin-right(px) для продукта.';
$_['entry_slideBy']               		= 'SlideBy Item';
$_['entry_slideBy_desc']            	= 'Navigation slide by x. page string can be set to slide by page.';
$_['entry_autoplay']	                = 'Авто перелистывание';
$_['entry_autoplay_desc']           	= 'Включить/отключить авто проигрование для слайдера';
$_['entry_autoplaySpeed']    			= 'Скорость автоматического воспроизведения';
$_['entry_autoplaySpeed_desc']    		= 'Скорость автоматического воспроизведения.';
$_['entry_startPosition']    			= 'Начальная позиция';
$_['entry_startPosition_desc']    		= 'Начальная позиция или URL-адрес Хэш-строка, например #id.';
$_['entry_mouseDrag']    				= 'Перемещенеи мышкой';
$_['entry_mouseDrag_desc']    			= 'Включение перемещения мышкой';
$_['entry_touchDrag']    				= 'Перемещение прикосновением';
$_['entry_touchDrag_desc']    			= 'Включение перемещения прикосновением';
$_['entry_pullDrag']    				= 'Перетаскивание';
$_['entry_pullDrag_desc']    			= 'Сдвиг сцены к краю';
$_['entry_dots']    					= 'Показывать пагинацию';
$_['entry_dots_desc']    				= 'Показать/Спрятать пагинацию для модуля.';
$_['entry_dotsSpeed']    				= 'Скорость пагинации';
$_['entry_dotsSpeed_desc']    			= 'Скорость пагинации.';
$_['entry_navs']    					= 'Показать навигацию';
$_['entry_navs_desc']    				= 'Показать/спрятать навигацию для модуля.';
$_['entry_navspeed']    				= 'Скорость навигации';
$_['entry_navspeed_desc']    			= 'Скорость навигации.';
$_['entry_effect']	                    = 'Эффект';
$_['entry_effect_desc']                	= 'Выберите эффект для модуля.';
$_['entry_duration']              		= 'Продолжительность';
$_['entry_duration_desc']             	= 'Определение продолжительности анимации. Larger = Медленнее.';
$_['entry_delay']                 		= 'Задержка';
$_['entry_delay_desc']                 	= 'Устанавливает таймер для задержки выполнения следующего элемента в очереди. Larger = Медленнее.';
$_['entry_pausehover']                  = 'Пауза при наведении';
$_['entry_pausehover_desc']             = 'Пауза при наведении мыши';
$_['entry_autoplay_timeout']            = 'Автоматический интервал ожидания';
$_['entry_autoplay_timeout_desc']       = 'Тайм-аут интервала автовоспроизведения для слайдера.';

//Tabs Advanced 
$_['entry_pre_text']                	= 'Пред-текст';
$_['entry_pre_text_desc']               = 'Верхний колонтитул модуля';
$_['entry_post_text']               	= 'Пост-текст';
$_['entry_post_text_desc']              = 'Нижний колонтитул модуля';
$_['entry_use_cache']                   = 'Кеширование';
$_['entry_use_cache_desc']              = 'Выберите, следует ли кэшировать содержимое этого модуля';
$_['entry_cache_time']                  = 'Время кеширования';
$_['entry_cache_time_desc']             = 'Время (в секундах) перед повторным кешированием модуля.';
// Help
$_['help_product']     					= '(Автозаполнение)';
$_['type_show_loadmore'] 				= 'Загрузить больше';
$_['type_show_slider'] 					= 'Слайдер';
// Error
$_['error_warning']          			= 'Внимание: Пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_permission'] 					= 'Внимание: У вас нет разрешения на изменение модуля категорий so!';
$_['error_name']       			        = 'Имя модуля должно быть от 3 до 64 символов!';
$_['error_head_name']       	        = 'Имя заголовка модуля должно быть от 3 до 64 символов!';
$_['error_category']   			        = 'Имя заголовка модуля должно быть от 3 до 64 символов!';
$_['error_category_depth']  	        = 'The number character format is required in this product category depth!';
$_['error_date_day']  			        = 'The number character format is required in this date new!';
$_['error_source_limit'] 		        = 'The number character format is required in this limitation!';
$_['error_product_feature']   	        = 'Имя заголовка модуля должно быть от 3 до 64 символов!';
$_['error_source_limit_cat'] 		    = 'The number character format is required in this limitation category!';
$_['error_title_maxlength'] 	        = 'The number character format is required in this title max maxlength!';
$_['error_cat_title_maxcharacs'] 	    = 'The number character format is required in this title max chars!';
$_['error_description_maxlength']       = 'The number character format is required in this description maxlength!';
$_['error_cat_sub_title_maxcharacs']    = 'The number character format is required in this title max chars !';
$_['error_width']      			        = 'The number character format > 0 is required in this width!';
$_['error_height']     			        = 'The number character format > 0 is required in this height!';
$_['error_width_cat']                   = 'The number character format > 0 is required in this category width!';
$_['error_height_cat']                  = 'The number character format > 0 is required in this category height!';
$_['error_nb_row']     			        = 'The number character format is required in this row!';
$_['error_margin'] 	                    = 'The number character format is required in this margin!';
$_['error_slideBy'] 	                = 'The number character format is required in this slideBy!';
$_['error_autoplay_timeout'] 	        = 'The number character format is required in this auto play timeout!';
$_['error_autoplaySpeed'] 	            = 'The number character format is required in this auto play speed!';
$_['error_smartSpeed'] 	                = 'The number character format is required in this smart speed!';
$_['error_startPosition'] 	            = 'The number character format is required in this start position!';
$_['error_navSpeed'] 	                = 'The number character format is required in this nav speed!';
$_['error_duration'] 	                = 'The number character format is required in this duration!';
$_['error_delay'] 	                    = 'The number character format is required in this delay!';
$_['error_placeholder_path']            = 'Строка выбора пути должна входить в изображение';