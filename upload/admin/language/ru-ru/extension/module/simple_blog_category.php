<?php
// Heading
$_['heading_title']       		= 'Simple Blog Категории';

// Text
$_['text_extension']            = 'Расширения';
$_['text_success']        		= 'Success: You have modified module simple blog category!';
$_['text_content_top']    		= 'Сверху';
$_['text_content_bottom'] 		= 'Внизу';
$_['text_column_left']    		= 'Слева';
$_['text_column_right']   		= 'Справа';
$_['text_category_related']		= 'Модуль категорий';
$_['text_edit']		            = 'Редактировать категории блога';

// Help
$_['help_search_article']       = 'Если включить, модуль поиска по статьям будет всключен.';

// Entry
$_['entry_search_article']		= 'Поиск статей:';
$_['entry_status']				= 'Статус:';
$_['entry_layout']        		= 'Расположение:';
$_['entry_position']      		= 'Позиция:';
$_['entry_sort_order']    		= 'Порядок сортировки:';

// Button
$_['button_add_module']			= 'Добавить модуль';

// Error
$_['error_permission']    		= 'Warning: You do not have permission to modify module simple blog category!';