﻿<?php

$_['heading_title']				= 'So Переключение цветов Pro';
$_['heading_title_so']			= 'So Переключение цветов Pro <b style="размер шрифта:10px; фон:#aaa; цвет:#fff; отступы: 3px;толщина шрифта:normal">версия 1.0.1</b>';

$_['entry_status']				= 'Статус';
$_['entry_option']				= 'Выберите опцию для применения цвета';
$_['entry_type']				= 'Образец цвета';
$_['entry_general_settings']	= 'Общие настройки';
$_['entry_product_list']		= 'Список продуктов';
$_['entry_product_page']		= 'Страница продакта';
$_['entry_enable']				= 'Включить';
$_['entry_icon_swatch_width']	= 'Высота иконок';
$_['entry_icon_swatch_height']	= 'Ширина иконок';

$_['text_edit']					= 'Редактировать цветные образцы Pro-модуля';
$_['text_status']				= 'Статус';
$_['text_status_help']			= 'Включить/отключить модуль';
$_['text_name']					= 'Имя';
$_['text_click']				= 'Нажатие';
$_['text_hover']				= 'Наведение';
$_['text_success']				= 'Готово: Вы модифицировали модуль So Color Swatches Pro!';
$_['error_permission']			= 'Внимание: У вас нет разрешения на модификацию модуля!';
$_['error_warning']				= 'Внимание: Вы должны тщательно проверить модуль';

$_['button_savestay']			= 'Сохранить & Остаться';