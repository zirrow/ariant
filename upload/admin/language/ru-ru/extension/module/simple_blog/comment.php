<?php
 // Heading
$_['heading_title']      		= 'Комментарии';

// Text
$_['text_success']				= 'Успех: вы изменили комментарии!';
$_['text_add']	                = 'Добавить комментарий';
$_['text_edit']	                = 'Редактировать комментарий';

// Columns
$_['column_article_title']		= 'Название статьи';
$_['column_author_name']		= 'Имя автора';
$_['column_status']				= 'Статус';
$_['column_date_added']			= 'Дата добавления';
$_['column_action']				= 'Действие';

// Buttons
$_['button_add_reply']			= 'Добавить комментарий';

// Help
$_['help_article']              = 'Название статьи должно быть выбрано из автодополнения.';

// Tab
$_['tab_comment']				= 'Комментарии';

// Entry
$_['entry_author']				= 'Имя автора:';
$_['entry_article']				= 'Название статьи:';
$_['entry_status']				= 'Статус:';
$_['entry_comment']				= 'Комментарий:';
$_['entry_reply_comment']		= 'Ответить';

// Error
$_['error_warning']				= 'Warning: Please check the form carefully for errors!';
$_['error_permission']  		= 'Warning: You do not have permission to modify comment!';
$_['error_author']				= 'Author name must be between 3 to 63 characters!';
$_['error_comment']				= 'Comment must be between 3 to 1000 characters!';
$_['error_article_title']  		= 'Article Title must be require!';
$_['error_article_title_not_found']	= 'Article Title not found in our list!';

$_['button_insert']			= 'Встравить';
