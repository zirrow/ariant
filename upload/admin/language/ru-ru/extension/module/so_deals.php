﻿<?php
// Heading
$_['heading_title']                 = 'So Сделки';
$_['heading_title_so']              = 'So Сделки <b style="размер шрифта:10px; фон:#aaa; цвет:#fff; отступы: 3px;толщина шрифта:normal">версия 2.2.0</b>';
$_['entry_button_clear_cache']      = 'Сбросить кешь';
// Text
$_['text_module']      			    = 'Модули';
$_['text_success']     			    = 'Готово: Вы модифицировали встроенный модуль!';
$_['text_success_remove']           = 'Готово: Сброс SO кеша успешно выполнен!';
$_['text_edit']        			    = 'Редактировать модуль So Популярные теги';
$_['text_create_new_module']	    = 'Создать новый модуль';
$_['text_edit_module'] 			    = 'Редактировать модуль';
$_['text_layout']      			    = 'После того, как вы установили и настроили модуль, вы можете добавить его в макет <a href="%s" class="alert-link">здесь</a>!';

// button
$_['entry_button_save']       		= 'Сохранить';
$_['entry_button_save_and_edit']    = 'Сохранить и редактировать';
$_['entry_button_save_and_new']    	= 'Сохранить и создать новый';
$_['entry_button_cancel']       	= 'Отменить';
$_['button_add_module'] 			= 'Добавить модель';
$_['entry_button_delete']       	= 'Удалить';

// value
$_['value_default']   		        = 'По умолчанию';
$_['value_default2']   		        = 'По умолчанию 2';
$_['value_default3']   		        = 'По умолчанию 3';
$_['value_home1']   		        = 'Главная 1';
$_['value_blank']			        = 'Новое окно';
$_['value_self']			        = 'То же окно';
$_['value_name']       		        = 'Название';
$_['value_model']       	        = 'Модель';
$_['value_price']       	        = 'Цена';
$_['value_quantity']       	        = 'Количество';
$_['value_rating']       	        = 'Реитинг';
$_['value_sort_order']              = 'Порядок сортировки';
$_['value_date_added']              = 'Дата добавления';
$_['value_sales']       	        = 'Распродажа';
$_['value_asc']       		        = 'Возростание';
$_['value_desc']       		        = 'Убывание';
$_['value_top']       		        = 'Сверху';
$_['value_under']       	        = 'Снизу';
$_['text_owl_carousel']    	        = 'Owl карусель 2';
$_['text_slick_slider']   	        = 'Slick слайдер';

$_['value_vertical']       	        = 'Вертикальный';
$_['value_horizontal']              = 'Горизонтальный';
//Default
$_['entry_include']                 = 'Добавить';
$_['entry_exclude']                 = 'Убрать';

// Entry
$_['entry_name']              			= 'Название модуля';
$_['entry_name_desc']         			= 'Модуль должен иметь название';
$_['entry_head_name']         			= 'Заголовок';
$_['entry_head_name_desc']    			= 'Заголовок';
$_['entry_display_title_module']       	= 'Показывать заголовок';
$_['entry_display_title_module_desc']   = 'Показывать заголовок модуля.';
$_['entry_status_desc']     			= 'Опубликовать/Спрятать модуль';

// Tabs
$_['entry_module']                  = 'Общие настройки';
$_['entry_source_option']           = 'Исходные опции';
$_['entry_tabs_option']             = 'Опции вкладок';
$_['entry_items_option']            = 'Опции товара';
$_['entry_image_option']            = 'Опции картинок';
$_['entry_effect_option']           = 'Опции эффектов';
$_['entry_advanced_option']         = 'Пробвинутые настройки';

//Tabs General
$_['entry_class_suffix']  		    = 'Class Суффикс модуля';
$_['entry_class_suffix_desc']  		= 'CSS Class который будет добавляться к модулям.';
$_['entry_open_link']  				= 'Открыть ссылку';
$_['entry_open_link_desc']  		= 'Тип открытия ссылок по клику';
$_['entry_include_js']  			= 'Подключить JS';
$_['entry_include_js_desc']  		= 'Тип подключаемых JS файлов.';
$_['entry_position_thumbnail']  	= 'Положение миниатюр';
$_['entry_position_thumbnail_desc'] = 'Расположение Миниатюры.';


$_['entry_column']     				= '# Колонки';
$_['entry_nb_column0_desc']         = 'Для устройств с шириной экрана >= 1200px или больше.';
$_['entry_nb_column1_desc']         = 'Для устройств с шириной экрана от >= 992px до < 1200px.';
$_['entry_nb_column2_desc']         = 'Для устройств с шириной экрана от >= 768px до < 992px.';
$_['entry_nb_column3_desc']         = 'Для устройств с шириной экрана от >= 480px до < 768px ';
$_['entry_nb_column4_desc']         = 'Для устройств с шириной экрана от < 480px ';
$_['entry_nb_row']                  = '# строк';
$_['entry_nb_row_desc']             = 'Число строк';

//Tabs Source options
$_['entry_field_product']         	= 'Field product' ;
$_['entry_category']              	= 'Категории';
$_['entry_category_desc']   	  	= 'Выберите категорию';
$_['entry_child_category']        	= 'Продукты дочерних категорий';
$_['entry_child_category_desc']  	= 'Добваить или убрать товары из дочерних категорий.';
$_['entry_category_depth']        	= 'Глубина категории';
$_['entry_category_depth_desc']  	= 'Количество возвращаемых категорий дочерних категорий.';

$_['entry_product_order']         	= 'Сортировка продуктов по';
$_['entry_product_order_desc']		= 'Сортировка продуктов';
$_['entry_ordering']              	= 'Порядок сортировки';
$_['entry_ordering_desc']   		= 'Выберите порядок, по которому вы хотите сортировать товары.';
$_['entry_source_limit']          	= 'Количество';
$_['entry_source_limit_desc'] 		= 'Количество отображаемых продуктов. Значение по умолчанию 0 отображает все продукты.';

$_['entry_display_feature']          	= 'Отобразить особенности продукта';
$_['entry_display_feature_desc'] 		= 'Отобразить особенности продукта.';

$_['entry_product_feature']          	= 'Особенности продукта';
$_['entry_product_feature_desc'] 		= 'Особенности продукта.';

//Tabs Items options
$_['entry_display_title']         		= 'Заголовок';
$_['entry_display_title_desc']   		= 'Показать название товара';
$_['entry_title_maxlength']       		= 'Макс. длина названия';
$_['entry_title_maxlength_desc']    	= 'Максимальная длина заголовка по символам. Введите значение 0, если вы хотите показать все. Введите целое число >= 0.';
$_['entry_display_description']   		= 'Отображение описания';
$_['entry_display_description_desc']    = 'Отображение описания товара';
$_['entry_description_maxlength'] 		= 'Макс. длина описания';
$_['entry_description_maxlength_desc']  = 'Максимальная длина описания по символам. Введите значение 0, если вы хотите показать все. Введите целое число >= 0.';
$_['entry_item_created_display']  		= 'Отображение даты создания';
$_['entry_item_created_display_desc']  	= 'Позволяет показывать/скрывать дату создания продукта';
$_['entry_display_price']     			= 'Показать цену';
$_['entry_display_price_desc']     		= 'Позволяет включить/выключить отображение цен на продукты.';
$_['entry_display_add_to_cart']     	= 'Отображать "В корзину"';
$_['entry_display_add_to_cart_desc']    = 'Отобразить кнопку "Добавить в корзину';
$_['entry_display_wishlist']     		= 'Отобразить "В желаемые"';
$_['entry_display_wishlist_desc']     	= 'Отобразить кнопку "В желаемые".';
$_['entry_display_compare']     		= 'Отобразить "В сравнения"';
$_['entry_display_compare_desc']     	= 'Отобразить кнопку "В сравнения".';

//Tabs Images options
$_['entry_product_image']               = 'Изображение продукта';
$_['entry_product_image_desc']          = 'Полное описание продукта';
$_['entry_product_get_image_data']     	= 'Получить картинку из вкладки "данные"';
$_['entry_product_get_image_data_desc'] = 'Get image product from tab data image.';
$_['entry_product_get_image_image']    	= 'Получить картинки из вкладки "изображения"';
$_['entry_product_get_image_image_desc']= 'Get image product from tab image.';
$_['entry_width'] 				        = 'Ширина';
$_['entry_width_desc'] 				    = 'Ширина картинки';
$_['entry_height'] 					    = 'Высота';
$_['entry_height_desc'] 			    = 'Высота картинки';
$_['entry_placeholder_path']      		= 'Путь к Placeholder (или Url)';
$_['entry_placeholder_path_desc']      	= 'Путь (или URL) изображения по умолчанию. Например: catalog/logo.png';
$_['entry_display_rating']     			= 'Отображать рейтинг';
$_['entry_display_rating_desc']     	= 'Разрегить/запретить отображения рейтинга продукта.';
$_['entry_display_sale']     	    	= 'Отобразить акции';
$_['entry_display_sale_desc']        	= 'Отобразить акции';
$_['entry_display_new']     	    	= 'Отобразить новинки';
$_['entry_display_new_desc']        	= 'Отобразить новинки';
$_['entry_date_day']     	    		= 'Время дней новинки';
$_['entry_date_day_desc']        		= 'Количество дней которые товар считается новым';
$_['entry_product_image_num']         	= 'Номер картинки продукта';
$_['entry_product_image_num_desc']     	= 'Показать номер изображения для продукта';

//Tabs Effect options
$_['effect_none'] 						= 'Отсутствует';
$_['effect_fadeIn'] 					= 'Ослабить';
$_['effect_zoomIn'] 					= 'Приблизить';
$_['effect_zoomOut'] 					= 'Отдалить';
$_['effect_slideLeft'] 					= 'Влево';
$_['effect_slideRight'] 				= 'Вправо';
$_['effect_slideTop'] 					= 'Вверх';
$_['effect_slideBottom'] 				= 'Вниз';
$_['effect_flip'] 						= 'Повернуть';
$_['effect_flipInX'] 					= 'Повернуть по горизонтали';
$_['effect_flipInY'] 					= 'Повернуть по вертикали';
$_['effect_starwars'] 					= 'Star war';
$_['effect_bounceIn'] 					= 'Перетащить';
$_['effect_bounceInUp'] 				= 'Перетащить вверх';
$_['effect_bounceInDown'] 				= 'Перетащить вниз';
$_['effect_pageTop'] 					= 'Вверх страницы';
$_['effect_pageBottom'] 				= 'Повернуть';

$_['entry_margin']                		= 'Отступ в право';
$_['entry_margin_desc']             	= 'Отступ в право (px) между продуктами.';
$_['entry_slideBy']               		= 'Перелистывать по товарам';
$_['entry_slideBy_desc']            	= 'Навигация слайда на x. строке страницы может быть установлена для перехода по страницам.';
$_['entry_autoplay']              		= 'Авто перелистывание';
$_['entry_autoplay_desc']           	= 'Включить/отключить авто проигрование для слайдера';
$_['entry_autoplayTimeout']      		= 'Автоматический интервал ожидания';
$_['entry_autoplayTimeout_desc']    	= 'Тайм-аут интервала автовоспроизведения для слайдера.';
$_['entry_autoplayHoverPause']    		= 'Пауза при наведении';
$_['entry_autoplayHoverPause_desc']    	= 'Пауза при наведении мышкой';
$_['entry_autoplaySpeed']    			= 'Скорость автовоспроизведения';
$_['entry_autoplaySpeed_desc']    		= 'Скорость автовоспроизведения.';
$_['entry_smartSpeed']    				= 'Умная скорость';
$_['entry_smartSpeed_desc']    			= 'Автоматическая скорость автовоспроизведения.';
$_['entry_startPosition']    			= 'Начальная позиция';
$_['entry_startPosition_desc']    		= 'Начальная позиция или URL-адрес Хэш-строка, например #id.';
$_['entry_mouseDrag']    				= 'Перетаскивание мышью';
$_['entry_mouseDrag_desc']    			= 'Включение перетаскивания мышью';
$_['entry_touchDrag']    				= 'Перетаскивание прикосновением';
$_['entry_touchDrag_desc']    			= 'Включить перетаскивание пальцем';
$_['entry_loop']    				    = 'Кольцо';
$_['entry_loop_desc']    			    = 'Включение кольца';
$_['entry_dots']    					= 'Показать разбивку на страницы';
$_['entry_dots_desc']    				= 'Показать/спрятать пагинацию';
$_['entry_navs']    					= 'Показать навигацию';
$_['entry_navs_desc']    				= 'Показать/спрятать навигацию';
$_['entry_button_page']    				= 'Кнопка';
$_['entry_button_page_desc']    		= 'Кнопка ведущая на страницу';
$_['entry_dotsSpeed']    				= 'Скорость пагинации';
$_['entry_dotsSpeed_desc']    			= 'Скорость пагинации.';
$_['entry_navspeed']    				= 'Скорость навигации';
$_['entry_navspeed_desc']    			= 'Скорость навигации.';
$_['entry_effect']                		= 'Выбор эффекта';
$_['entry_effect_desc']                	= 'Выберите эффект для модуля.';
$_['entry_duration']              		= 'Продолжительность';
$_['entry_duration_desc']              	= 'Определение продолжительности анимации. Larger = Медленнее.';
$_['entry_delay']                 		= 'Задержка';
$_['entry_delay_desc']                 	= 'Устанавливает таймер для задержки вывода следующего элемента из очереди. Larger = Медленнее.';

//Tabs Advanced 
$_['entry_store_layout']                = 'Макет магазина';
$_['entry_store_layout_desc']           = 'Select Store layout show';
$_['entry_pre_text']                	= 'Пред-текст';
$_['entry_pre_text_desc']               = 'Верхний колонтитул модуля';
$_['entry_post_text']               	= 'Пост-текст';
$_['entry_post_text_desc']              = 'Нижний колонтитул модуля';
$_['entry_use_cache']                   = 'Кеширование';
$_['entry_use_cache_desc']              = 'Выберите, следует ли кэшировать содержимое этого модуля';
$_['entry_cache_time']                  = 'Время кеширвоания';
$_['entry_cache_time_desc']             = 'Время (в секундах) перед повторным кешированием модуля.';

// Help
$_['help_product']                      = '(Автодополнение)';
$_['entry_status']                      = 'Статус';
// Error
$_['error_warning']          	= 'Внимание: Пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_permission'] 			= 'Внимание: У вас нет разрешения на модификацию модуля so deals!';
$_['error_name']       			= 'Имя модуля должно быть от 3 до 64 символов!';
$_['error_head_name']       	= 'Имя заголовка модуля должно быть от 3 до 64 символов!';
$_['error_category']   			= 'Это поле не должно быть пустым!';
$_['error_category_depth']  	= 'The number character format is required in this category depth!';
$_['error_source_limit'] 		= 'The number character format is required in this limitation!';
$_['error_product_feature']   	= 'Это поле не должно быть пустым!';
$_['error_title_maxlength'] 	= 'The number character format is required in this title maxlength!';
$_['error_description_maxlength'] = 'The number character format is required in this description maxlength!';
$_['error_width']      			= 'The number character format from 1 to 5000 is required in this width!';
$_['error_height']     			= 'The number character format from 1 to 5000 is required in this height!';
$_['error_placeholder_path']    = 'Строка выбора пути должна входить в изображение, например: nophoto.png';
$_['error_margin']     			= 'The number character format is required in this margin!';
$_['error_slideBy']     		= 'The number character format is required in this slide by!';
$_['error_smartSpeed']     		= 'The number character format is required in this smart speed!';
$_['error_startPosition']     	= 'The number character format is required in this start position!';
$_['error_dotsSpeed']     		= 'The number character format is required in this pagination speed!';
$_['error_navSpeed']     		= 'The number character format is required in this navigation speed!';
$_['error_duration']     		= 'The number character format is required in this duration!';
$_['error_delay']     			= 'The number character format is required in this delay!';
$_['error_autoplayTimeout']     = 'The number character format is required in this Autoplay Timeout!';
$_['error_autoplaySpeed']     	= 'The number character format is required in this Autoplay Speed!';
$_['error_date_day']  			= 'The number character format is required in this date new!';


