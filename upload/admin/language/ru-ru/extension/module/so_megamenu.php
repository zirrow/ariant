﻿<?php
// Heading
$_['heading_title']             = 'So МегаМеню';
$_['heading_title_so']          = 'So МегаМеню <b style="размер шрифта:10px; фон:#aaa; цвет:#fff; отступы: 3px;толщина шрифта:normal">версия 2.2.0</b>';
$_['entry_button_clear_cache']  = 'Сбросить кеш';
$_['text_success_remove']       = 'Готово: Кеш сброшен успешно!';
// Text

$_['text_button_add_module']      		= ' Добавить модуль';
$_['text_creat_new_item']      			= 'Добавить';
$_['text_expand_all']      				= 'Раскрыть все';
$_['text_collapse_all']      			= 'Свернуть все';
$_['text_edit_item']      				= 'Редактировать (ID:';
$_['text_name']      					= 'Имя';
$_['text_description']      			= 'Описание';
$_['text_label_item']      				= 'Метка пункта';
$_['text_icon_font']      				= 'Иконка шрифта';
$_['text_type_link']      				= 'Тип ссылки';
$_['text_categories']      				= 'Категории';
$_['text_all_categories']      			= 'Все категории';
$_['text_link_in_new_window']      		= 'Открыть ссылку в новом окре';
$_['text_disabled']      				= 'Отключено';
$_['text_enabled']      				= 'Включено';
$_['text_status']      					= 'Статус';
$_['text_class_menu']      				= 'Class';
$_['text_position']      				= 'Позиция';
$_['text_left']      					= 'Слева';
$_['text_right']      					= 'Справа';
$_['text_submenu_width']      			= 'Ширина подменю';
$_['text_example']      				= 'Привер: 100%, 250px';
$_['text_display_submenu_on']      		= 'Отображение подменю включено';
$_['text_content_config']      			= 'Конфигурация содержимого меню';
$_['text_parent_config']      			= 'Настройка родительского меню';
$_['text_content_item']      			= 'Элемент контента - содержимое этих полей будет отображаться только в том случае, если меню будет установлено как подменю.';
$_['text_parent_item']      			= 'Элемент контента - содержимое этих полей будет отображаться только в том случае, если меню будет установлено как родительское (меню уровень 1).';
$_['text_content_width']      			= 'Ширина контента';
$_['text_content_type']      			= 'Тип контента';
$_['text_name']       					= 'Название модуля';
$_['text_name_desc']         			= 'Модуль должен иметь название';
$_['text_basic_configuration']     	    = 'Базовая настройка';
$_['text_design_configuration']     	= 'Настройка дизайна';
$_['text_orientation']     			    = 'Ореинтация';
$_['text_number_load_vertical']     	= 'Номер вертикальная загрузка';
$_['text_navigation_text']     		    = 'Заголовок навигации';
$_['text_expand_menu_bar']     		    = 'Развернуть панель меню на всю ширину?';
$_['text_home_item']     			    = 'Главная страница';
$_['text_home_text']     			    = 'Текст на главной';
$_['text_jquery_animations']     	    = 'jQuery анимация';
$_['text_animation']     			    = 'Анимация';
$_['text_animation_time']     		    = 'Время анимации';

$_['text_module']      					= 'Модули';
$_['text_success']     					= 'Готово: Вы изменили so модуль МегаМеню!';
$_['text_success_menu_delete']    		= 'Готово: Вы изменили менеджер меню!';
$_['text_success_menu_add']    			= 'Готово: Вы добавили менеджер меню!';
$_['text_success_menu_edit']    		= 'Готово: Вы редактировали менеджер меню!';
$_['text_success_menu_item_delete']    	= 'Готово: Вы изменили menu item manager!';
$_['text_success_menu_item_add']    	= 'Готово: Вы добавили menu item manager!';
$_['text_success_menu_item_edit']    	= 'Готово: Вы редактировали menu item manager!';
$_['text_edit']        					= 'Редактировать модуль Мега Меню';

$_['entry_display_title_module']     	= 'Показать заголовок';
$_['entry_menu_manager']     	        = 'Менеджер меню';
$_['entry_menu_items']     		        = 'Menu Items Manager';
$_['entry_head_name']     		        = 'Название';
$_['entry_description_name']     		= 'Описание';
$_['entry_please_select']		        = '--Выберите--';

//Menu manager
$_['entry_menu_groups_id']       			= 'Id';
$_['entry_menu_groups_title']       		= 'Название';
$_['entry_menu_groups_title_desc']      	= 'Название';
$_['entry_menu_groups_status']       		= 'Статус';
$_['entry_menu_groups_action']       		= 'Действие';
$_['entry_button_edit']       				= 'Редактировать';
$_['entry_text_confirm']       				= 'Вы уверены?';
$_['entry_button_delete']       			= 'Удалить';
$_['entry_menu_groups_content']       		= 'Контент';
$_['entry_menu_groups_content_desc']    	= 'Контент';
$_['entry_menu_groups_theme']       		= 'Выберите шаблон';
$_['entry_menu_groups_theme_desc']      	= 'Выберите шаблон';
$_['entry_menu_groups_effect']       		= 'Выберите эффект';
$_['entry_menu_groups_effect_desc']      	= 'Выберите эффект';
$_['entry_menu_groups_duration']       		= 'Тип эффекта';
$_['entry_menu_groups_duration_desc']      	= 'Тип эффекта';
$_['entry_menu_groups_startLevel']       	= 'Начальный уровень';
$_['entry_menu_groups_startLevel_desc']     = 'Начальный уровень';
$_['entry_menu_groups_endLevel']       		= 'Конечный уровень';
$_['entry_menu_groups_endLevel_desc']     	= 'Конечный уровень';

$_['entry_menu_groups_theme_']     	        = 'Шаблон';

//Menu item
$_['entry_menu_item_groups_id']       			= 'Id';
$_['entry_menu_item_groups_title']       		= 'Название';
$_['entry_menu_item_groups_title_desc']       	= 'Название';
$_['entry_menu_item_groups_groups']       		= 'Группы меню';
$_['entry_menu_item_groups_level']       		= 'Уровень меню';
$_['entry_menu_item_groups_status']       		= 'Статус';
$_['entry_menu_item_groups_action']       		= 'Действие';
//entry
$_['entry_menu_item_groups_description']       	= 'Описание';
$_['entry_menu_item_groups_description_desc']   = 'Описание';
$_['entry_menu_item_groups_align']       		= 'Выравнивание';
$_['entry_menu_item_groups_align_desc']       	= 'Выравнивание';
$_['entry_menu_item_groups_menu_group']       	= 'Группа меню';
$_['entry_menu_item_groups_menu_group_desc']    = 'Группа меню';
$_['entry_menu_item_groups_parent_item']       		= 'Товары для родителей';
$_['entry_menu_item_groups_parent_item_desc']       = 'Товары для родителей';
$_['entry_menu_item_groups_column']       			= 'Номер столбца';
$_['entry_menu_item_groups_column_desc']       		= 'Номер столбца';
$_['entry_menu_item_groups_custom_class']       	= 'Кастомный Class';
$_['entry_menu_item_groups_custom_class_desc']      = 'Кастомный Class';
$_['entry_menu_item_groups_icon']       			= 'Иконка';
$_['entry_menu_item_groups_icon_desc']       		= 'Иконка';
$_['entry_menu_item_groups_target_window']      	= 'Целевое окно';
$_['entry_menu_item_groups_target_window_desc']     = 'Целевое окно';
$_['entry_menu_item_groups_menu_type']      		= 'Тип меню';
$_['entry_menu_item_groups_menu_type_desc']      	= 'Тип меню';
$_['entry_menu_item_groups_data_type']      		= 'Тип данных';
$_['entry_menu_item_groups_data_type_desc']      	= 'Тип данных';
$_['entry_menu_item_groups_product']      			= 'Продукт';
$_['entry_menu_item_groups_product_desc']      		= 'Продукт';
$_['entry_menu_item_groups_category']      			= 'Категория';
$_['entry_menu_item_groups_category_desc']      	= 'Категория';
$_['entry_menu_item_groups_content']      			= 'Контент';
$_['entry_menu_item_groups_content_desc']      		= 'Контент';
// cache
$_['entry_use_cache']                   = 'Кеширование';
$_['entry_use_cache_desc']              = 'Выберите, следует ли кэшировать содержимое этого модуля';
$_['entry_cache_time']                  = 'Время кеширвоания';
$_['entry_cache_time_desc']             = 'Время (в секундах) перед повторным кешированием.';
// Help
$_['help_product']     = '(Автокомплит)';
$_['entry_status']     = 'Статус';
// Error
$_['error_permission'] = 'Внимание: У вас нет разрешения на изменение встроенного модуля!';
$_['error_name']       = 'Имя модуля должно быть от 3 до 64 символов!';
$_['error_title']      = 'Заголовок должен быть от 3 до 64 символов!';
$_['error_width']      = 'Требуется ширина!';
$_['error_height']     = 'Требуется высота!';
$_['error_description_name']     = 'Требуется описание!';
$_['error_name']       = 'Требуется имя!';

// Heading Goes here:
//$_['heading_title']    = 'So Мега Меню';
// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Готово: Вы изменили модуль МегаМеню!';
$_['text_warning']     = 'Внимание: Изменения не были сохранены! Убедитесь, что вы хорошо заполнили все поля.';
$_['error_name']       = 'Имя модуля должно быть от 3 до 64 символов!';
// Error
$_['error_permission'] = 'Внимание: У вас нет разрешения на модификацию модуля МегаМеню!';

