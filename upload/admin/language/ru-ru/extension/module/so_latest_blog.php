<?php

// Text
$_['text_install_message']  = 'Отсутствуют таблицы базы данных для этого расширения, пожалуйста, установите «Simple Blog»!';
// Error
$_['error_database']        = 'База данных не найдена!';
$_['entry_button_clear_cache']  = 'Сбросить кеш';


// Heading
$_['heading_title']    = 'So Последний блог';
$_['heading_title_so']    = 'So Последний блог <b style="размер шрифта:10px; фон:#aaa; цвет:#fff; отступы: 3px;толщина шрифта:normal">версия 2.1.2</b>';

// Text
$_['text_module']      			= 'Модули';
$_['text_success']     			= 'Готово: Вы изменили встроенный модуль!';
$_['text_success_remove']       = 'Готово: SM Кеш сброшен успешно!';
$_['text_edit']        			= 'Редактировать So модуль Последний блог';
$_['text_create_new_module']	= 'Создать новый модуль';
$_['text_edit_module'] 			= 'Редактировать модуль ';
$_['text_layout']      			= 'После того, как вы установили и настроили модуль, вы можете добавить его в макет <a href="%s" class="alert-link">здесь</a>!';
// button
$_['entry_button_save']       		= 'Сохранить';
$_['entry_button_save_and_edit']    = 'Сохранить и Редактировать';
$_['entry_button_save_and_new']    	= 'Сохранить и Создать новый';
$_['entry_button_cancel']       	= 'Отменить';
$_['button_add_module'] 			= 'Добавить модуль';
$_['entry_button_delete']       	= 'Удалить';

// value
$_['value_blank']			= 'Новое окно';
$_['value_self']			= 'Такое же окно';
$_['value_loadmore']		= 'Загрузить больше';
$_['value_slider']			= 'Слайдер';

$_['value_article_title']		= 'Заголовок статьи';
$_['value_author_name']       	= 'Имя автора';
$_['value_status']       		= 'Статус';
$_['value_sort_order']      	= 'Порядок сортировки';
$_['value_date_added']      	= 'Дата добавления';

$_['value_asc']       		= 'По возрастанию';
$_['value_desc']       		= 'По убыванию';
// Entry
$_['entry_name']       					= 'Название модуля';
$_['entry_name_desc']       			= 'Модуль должен иметь название.';
$_['entry_head_name']         			= 'Заголовок верхнего колонтитула';
$_['entry_head_name_desc']    			= 'Озаглавить верхний колонтитул.';
$_['entry_display_title_module']       	= 'Показать заголовок модуля';
$_['entry_display_title_module_desc']   = 'Показать заголовок модуля.';
$_['entry_status']     					= 'Статус';
$_['entry_status_desc']     			= 'Опубликовать / Отменить публикацию модуля';

// Options
$_['entry_module']     		= 'Общие настройки';
$_['entry_category_option'] = 'Настройки категорий';
$_['entry_blog_option']  	= 'Настройки блога';
$_['entry_image_option']    = 'Настройки изображений';
$_['entry_effect_option']   = 'Настройки эффектов';
$_['entry_advanced_option']     = 'Рекомендуемые настройки';

// General Options
$_['entry_class_suffix']  			= 'Модуль суффикс Class';
$_['entry_class_suffix_desc']		= 'Суффикс Class для модуля.';
$_['entry_open_link']  				= 'Открыть ссылку';
$_['entry_open_link_desc']  		= 'Тип показывается когда вы нажимаете на ссылку.';
$_['entry_limit']     				= 'Ограничение';
$_['entry_limit_desc'] 				= 'Количество отобрааемых блогов. Значение по умолчанию 0 отображает все блоги.';
$_['entry_button_page']  		    = 'Страница кнопок';
$_['entry_button_page_desc']        = 'Отображение типа страницы кнопок';
$_['entry_column']     		        = '# Колонки';
$_['entry_nb_row']                  = '# Ряды';
$_['entry_nb_row_desc']             = 'Количество рядов ';
$_['entry_nb_column0_desc']         = 'Для устройств с шириной экрана >= 1200px и больше.';
$_['entry_nb_column1_desc']         = 'Для устройств с шириной экрана от >= 992px до < 1200px.';
$_['entry_nb_column2_desc']         = 'Для устройств с шириной экрана от >= 768px до < 992px.';
$_['entry_nb_column3_desc']         = 'Для устройств с шириной экрана от >= 480px до < 768px ';
$_['entry_nb_column4_desc']         = 'Для устройств с шириной экрана от < 480px ';
$_['entry_type_show'] 		        = 'Тип показа';
$_['entry_type_show_desc'] 	        = 'Выбрать тип.';
$_['entry_nb_row']                  = '# Ряды';
$_['entry_nb_row_desc']             = 'Количество рядов ';

// Category Options
$_['entry_category']     				= 'Категория';
$_['entry_category_desc']   			= 'Выбрать категорию';
$_['entry_child_category']  			= 'Категория товаров для детей';
$_['entry_child_category_desc']  		= 'Добавить или убрать продукты из Категорий товаров для детей.';
$_['entry_include']     				= 'Добавить';
$_['entry_exclude']     				= 'Убрать';
$_['entry_category_depth']  			= 'Глубина категории';
$_['entry_category_depth_desc']  		= 'Количество уровней категории для детей для возврата.';
$_['entry_sort']   						= 'Сортировать по';
$_['entry_sort_desc']					= 'Сортировать по.';
$_['entry_order']     					= 'Направление заказа';
$_['entry_order_desc']   				= 'Выберите направление, по которому вы хотите заказать статью.';

// blogs Options
$_['entry_display_title']   			= 'Отобразить заголовок';
$_['entry_display_title_desc']   		= 'Отобразить заголовок товара.';
$_['entry_title_maxlength']     		= 'Максимальная длина заголовка';
$_['entry_title_maxlength_desc']     	= 'Максимальная длина заголовка по символам. Введите значение 0, если вы хотите показать все. Введите целое число >= 0.';
$_['entry_display_description']     	= 'Отобразить описание';
$_['entry_display_description_desc']    = 'Отобразить описание товара.';
$_['entry_description_maxlength']     	= 'Максимальная длина описания';
$_['entry_description_maxlength_desc']  = 'Максимальная длина описания по символам. Введите значение 0, если вы хотите показать все. Введите целое число >= 0.';
$_['entry_display_author']     			= 'Отобразить автора';
$_['entry_display_author_desc']    		= 'Отобразить автора товара.';
$_['entry_display_comment']     		= 'Отобразить комментарии';
$_['entry_display_comment_desc']    	= 'Отобразить комментарии к товару.';
$_['entry_display_view']     			= 'Отобразить обзор';
$_['entry_display_view_desc']    	    = 'Отобразить обзор товара.';
$_['entry_display_date_added']     		= 'Отобразить дату добавления';
$_['entry_display_date_added_desc']    	= 'Отобразить дату добавления товара.';
$_['entry_display_readmore']     		= 'Отобразить Читать больше';
$_['entry_display_readmore_desc']    	= 'Отобразить Читать больше о товаре.';
$_['entry_readmore_text']     			= 'Текст Читать больше';
$_['entry_readmore_text_desc']  		= 'Текст Читать больше.';
$_['entry_product_image_num']         	= 'Номер изображения продукта';
$_['entry_product_image_num_desc']     	= 'Показать номер изображения продукта'; 

// Image Options
$_['entry_blog_image']     				= 'Показать блог изображений';
$_['entry_blog_image_desc']     		= 'Показать блог изображений.';
$_['entry_blog_get_image']     			= 'Получить изображение из изображения столбцов';
$_['entry_blog_get_image_desc'] 		= 'Получить изображение блога из изображения столбцов .';
$_['entry_blog_get_featured_image']    	= 'Получить изображение из столбцов миниатюр записи';
$_['entry_blog_get_featured_image_desc']= 'Получить изображение блога из столбцов миниатюр записи.';
$_['entry_width'] 						= 'Ширина';
$_['entry_width_desc'] 					= 'Ширина изображения';
$_['entry_height'] 						= 'Высота'; 
$_['entry_height_desc'] 				= 'Высота изображения';
$_['entry_blog_placeholder_path'] 	= 'Путь к изображению (или Url)'; 
$_['entry_blog_placeholder_path_desc']= 'Путь к изображению по умолчанию (или  Url)';

// Effect Options
$_['effect_none'] 						= 'Нет';
$_['effect_fadeIn'] 					= 'Ослабить';
$_['effect_zoomIn'] 					= 'Приблизить';
$_['effect_zoomOut'] 					= 'Отдалить';
$_['effect_slideLeft'] 					= 'Влево';
$_['effect_slideRight'] 				= 'Враво';
$_['effect_slideTop'] 					= 'Вверх';
$_['effect_slideBottom'] 				= 'Вниз';
$_['effect_flip'] 						= 'Повернуть';
$_['effect_flipInX'] 					= 'Повернуть по горизонтали';
$_['effect_flipInY'] 					= 'Повернуть по вертикали';
$_['effect_starwars'] 					= 'Star war';
$_['effect_bounceIn'] 					= 'Перетащить';
$_['effect_bounceInUp'] 				= 'Перетащить вверх';
$_['effect_bounceInDown'] 				= 'Перетащить вниз';
$_['effect_pageTop'] 					= 'Вверх страницы';
$_['effect_pageBottom'] 				= 'Повернуть';

$_['entry_margin']                		= 'Отступ вправо';
$_['entry_margin_desc']             	= 'Отступ вправо (px) между продуктами.';
$_['entry_slideBy']               		= 'Перелистывать по товарам';
$_['entry_slideBy_desc']            	= 'Навигация слайда на x. строке страницы может быть установлена для перехода по страницам.';
$_['entry_autoplay']	                = 'Автовоспроизведение';
$_['entry_autoplaySpeed']    			= 'Скорость автовоспроизведения';
$_['entry_autoplaySpeed_desc']    		= 'Скорость автовоспроизведения.';
$_['entry_smartSpeed']    				= 'Умная скорость';
$_['entry_smartSpeed_desc']    			= 'Автоматическая скорость автовоспроизведения.';
$_['entry_startPosition']    			= 'Начальная позиция';
$_['entry_startPosition_desc']    		= 'Начальная позиция или URL хэш-строки как #id.';
$_['entry_mouseDrag']    				= 'Перетаскивание мышью';
$_['entry_mouseDrag_desc']    			= 'Перетаскивание мышью включено';
$_['entry_touchDrag']    				= 'Touch перетаскивание';
$_['entry_touchDrag_desc']    			= 'Touch перетаскивание включено';
$_['entry_dots']    					= 'Показать разбивку на страницы';
$_['entry_dots_desc']    				= 'Разрешить отображение / скрытие разбивки на страницы для модуля';
$_['entry_dotsSpeed']    				= 'Скорость разбивки на страницы';
$_['entry_dotsSpeed_desc']    			= 'Скорость разбивки на страницы.';
$_['entry_navs']    					= 'Показать навигацию';
$_['entry_navs_desc']    				= 'Разрешить отображение / скрытие разбивки на страницы для модуля';
$_['entry_navspeed']    				= 'Скорость навигации';
$_['entry_navspeed_desc']    			= 'Скорость навигации.';
$_['entry_display_nav']	                = 'Отобрзить навигацию';
$_['entry_effect']	                    = 'Эффект';
$_['entry_loop']	                    = 'Петля';
$_['entry_loop_desc']	                = 'Отобрзить петлю';
$_['entry_duration']              		= 'Продолжительность';
$_['entry_duration_desc']             	= 'Определение продолжительности анимации. Больше = медленнее.';
$_['entry_delay']                 		= 'Задержка';
$_['entry_delay_desc']                 	= 'Устанавливить таймер для задержки выполнения следующего элемента в очереди. Больше = медленнее.';
$_['entry_pausehover']                  = 'Пауза при наведении';
$_['entry_pausehover_desc']             = 'Пауза при наведении мыши';
$_['entry_autoplay_timeout']            = 'Автоматический интервал ожидания';
$_['entry_autoplay_timeout_desc']       = 'Тайм-аут интервала автовоспроизведения для слайдера.';

//Tabs Advanced 
$_['entry_pre_text']                	= 'Пред-текст';
$_['entry_pre_text_desc']               = 'Верхний колонтитул модуля';
$_['entry_post_text']               	= 'Пост-текст';
$_['entry_post_text_desc']              = 'Нижний колонтитул модуля';
$_['entry_use_cache']                   = 'Кеширование';
$_['entry_use_cache_desc']              = 'Выберите, следует ли кэшировать содержимое этого модуля ';
$_['entry_cache_time']                  = 'Время кеширования';
$_['entry_cache_time_desc']             = 'Время (в секундах) перед повторным кешированием модуля.';
$_['entry_store_layout']                = 'Макет магазина';
$_['entry_store_layout_desc']           = 'Выберите Показать расположение магазина';
$_['value_default']   		= 'По умолчанию';

// Help
$_['help_blog']     					= '(Автозаполнение)';
$_['type_show_simple']                  = 'Простой';
$_['type_show_slider']                  = 'Слайдер';

// Error
$_['error_warning']          	= 'Внимание: Пожалуйста, внимательно проверьте форму на наличие ошибок!';
$_['error_permission'] 			= 'Внимание: У вас нет разрешения на изменение встроенного модуля!';
$_['error_name']       			= 'Имя модуля должно быть от 3 до 64 символов!';
$_['error_head_name']       	= 'Имя заголовка модуля должно быть от 3 до 64 символов!';
$_['error_category']   			= 'Это поле не должно быть пустым!';
$_['error_category_depth']  	= 'The number character format is required in this category depth!';
$_['error_limit'] 				= 'The number character format is required in this limit!';
$_['error_title_maxlength'] 	= 'The number character format is required in this title maxlength!';
$_['error_description_maxlength'] = 'The number character format is required in this description maxlength!';
$_['error_readmore_text'] = 'Это поле не должно быть пустым!';
$_['error_width']      = 'The number character format is required in this width!';
$_['error_height']     = 'The number character format is required in this height!';

$_['error_nb_row']     			= 'The number character format is required in this row!';
$_['error_autoplay_timeout']   = 'The number character format is required in this auto play timeout!';
$_['error_autoplay_speed']   = 'The number character format is required in this auto play speed!';
$_['error_duration']     		= 'The number character format is required in this duration!';
$_['error_delay']     			= 'The number character format is required in this delay!';
$_['error_margin']     			= 'The number character format is required in this margin!';
$_['error_slideBy']     		= 'The number character format is required in this slide by!';
$_['error_smartSpeed']     		= 'The number character format is required in this smart speed!';
$_['error_startPosition']     	= 'The number character format is required in this start position!';
$_['error_autoplaySpeed']     	= 'The number character format is required in this Autoplay Speed!';
$_['error_dotsSpeed']     		= 'The number character format is required in this pagination speed!';
$_['error_navSpeed']     		= 'The number character format is required in this navigation speed!';
$_['error_blog_placeholder_path']    = 'Путь к изображению должен войти в изображение, например: nophoto.png';