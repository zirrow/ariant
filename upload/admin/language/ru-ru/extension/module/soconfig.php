<?php
$_['heading_title_normal']  = 'Панель управления темами ';
$_['heading_title'] 		= '<i class="fa fa-wrench" style="    размер шрифта: 14px;поле: 0 5px;отступ: 5px;фон: #56a03e;цвет: белый; граница радиуса: 3px;"></i></i> Панель управления темами';
$_['text_module']     		= 'Модули';
$_['module_intro'] 			= 'Редактируйте функции темы SO здесь';
$_['theme_version']         = 'Версия 1.0.8';

// Text
$_['text_success']          = 'Готово: Вы изменили модуль!';

// Error
$_['error_permission']      = 'Внимание: У вас нет разрешения на модификацию модуля!';
$_['error_nameColor']       = 'Это поле не должно быть пустым';
$_['error_warning']         = 'Внимание: Пожалуйста, внимательно проверьте форму на наличие ошибок!';


$_['text_content_top']      = 'Вверх';
$_['text_content_bottom']   = 'Вниз';
$_['text_column_left']      = 'Влево';
$_['text_column_right']     = 'Вправо';
$_['text_default']          = 'По умолчанию';
$_['text_image_manager']    = 'Менеджер изображений';
$_['text_browse']           = 'Просмотреть';
$_['text_clear']            = 'Очистить';

$_['text_content_footer']    = 'Нижний колонтитул';
// Column
$_['column_slide_id']        = 'Slide ID';
$_['column_image_left']      = 'Изображение слева';
$_['column_image_right']     = 'Изображение справа';
$_['column_url_left']        = 'ссылка слева';
$_['column_url_right']       = 'Ссылка справа';
$_['column_sort_order']      = 'Порядок сортировки';
$_['column_action']          = 'Действие';


// Entry
$_['entry_image_left']       = 'Изображение слева:';
$_['entry_image_right']      = 'Изображение справа:';
$_['entry_url_left']         = 'URL слева:';
$_['entry_url_right']        = 'URL справа:';
$_['entry_sort_order']       = 'Порядок сортировки:';
$_['entry_status']           = 'Статус:';
$_['entry_position']         = 'Расположение:';
$_['text_edit']           	 = 'Редактирование панели управления темами';

$_['text_yes']           				= 'Да';
$_['text_no']           				= 'Нет';
$_['entry_standard']           			= 'Стандарт';
$_['entry_google_font']           		= 'Шрифты Google';
$_['entry_custom_block']                = 'Пользовательский блок';

$_['entry_title_label']                 = 'Заголовок :';
$_['entry_custom_column']               = 'Содержание :';
$_['entry_my_account']                  = 'Ссылка на аккаунт :';
$_['entry_contact_us']                  = 'Ссылка на контакт :';
$_['entry_google_url']                  = 'Google URL :';
$_['entry_google_url_help']             = 'Пример: http://fonts.googleapis.com/css?family=Roboto:400,500,700 <a href="https://www.google.com/fonts">&#8658; Показать больше</a>';
$_['entry_google_family']               = 'Google Family :';
$_['entry_google_family_help']          = 'Пример: Roboto, sans-serif;';
$_['entry_catalog_column_help']         = '(установить количество столбцов для устройств)';


// Main_tabs Options
$_['maintabs_general']                 	= '<i class="fa fa-cog"></i> <span>Общий </span>';
$_['maintabs_layout']                 	= '<i class="fa fa-th"></i> <span> Макет</span>';
$_['maintabs_products']                 = '<i class="fa fa-file-text"></i> <span>Страницы</span>';
$_['maintabs_fonts']                	= '<i class="fa fa-font"></i> <span>Шрифты</span>';
$_['maintabs_custom']                	= '<i class="fa fa-pencil-square"></i> <span>Кастом</span>';
$_['maintabs_social']                	= '<i class="fa fa-facebook"></i> <span>Соц</span>';
$_['maintabs_sampledata']               = '<i class="fa fa-database"></i> <span>Пример данных</span>';
$_['maintabs_advanced']                	= '<i class="fa fa-wrench"></i> <span>Рекомендуемый</span>';

// General Options
$_['general_tab_general']               = 'Общие';
$_['general_tab_header']                = 'Шапка сайта';
$_['general_tab_footer']                = 'Футер';
$_['general_tab_mainmenu']              = 'Главное меню';
$_['general_tab_language']              = 'Смена языка';

$_['general_res_layout']                = 'Адаптивный дизайн';
$_['general_back_top']                	= 'Вернуться к началу';
$_['general_cpanel']                	= 'Управление Cpanel';
$_['general_copyright']                 = 'Копирайт';
$_['general_logo_pos']                 	= 'Расоложение логотипа';
$_['general_logo_pos_help']             = 'Другие элементы колонтитула будут установлены с каждой опцией.';

// Fonts Options
$_['fonts_body']                		= 'Настройка шрифта Body';
$_['fonts_menu']             			= 'Настройка шрифта меню';
$_['fonts_heading']                		= 'Настройка верхнего колонтитула';
$_['fonts_custome']                		= 'Настройка пользовательских шрифтов';

// Pages Options
$_['pages_pro_listings']                = 'Категория продукта';
$_['pages_pro_label']                	= 'Лейбл продукта';
$_['pages_pro_detail']                	= 'Страница продукта';
$_['pages_comingsoon']                	= 'Скоро в продаже';
$_['product_catalog_title']             = 'Показать категорию заголовков';
$_['product_catalog_show']              = 'Показать информацию о категории';
$_['product_catalog_mode']              = 'Количество столбцов в категориях';
$_['product_catalog_column']            = 'Количество ячеек в сетке с продуктами';
$_['product_catalog_refine']            = 'Показать подкатегорию';
$_['product_catalog_refine_col']        = 'Количество столбцов в подкатегориях';
$_['show_product_item_desc']            = 'Показать описание товара';
$_['product_catalog_col_position']      = 'Позиция отображения столбцов ';
$_['product_catalog_col_type']          = 'Тип отображения столбцов ';
$_['product_catalog_sidebar_sticky']    = 'Прикрепленная боковая панель';

// Custom Options
$_['custom_tab_css_input']        		= 'Кастомный CSS';
$_['custom_tab_css_file']        		= 'CSS файл';
$_['custom_tab_js_input']        		= 'Кастомный JS';
$_['custom_tab_js_file']        		= 'JS файл';

// Custom Slide Blocks
$_['slide_tab_facebook']        		= 'Facebook Box';
$_['slide_tab_twitter']        			= 'Twitter Box';
$_['slide_tab_video']        			= 'Video Box';
$_['slide_tab_custom']        			= 'Custom Box';


$_['entry_layout_sub_help']             = 'По умолчанию ваучеры имеют адаптивный дизайн, но вы можете выбрать дополнительные параметры';
$_['entry_sidebars_width']              = 'Ширина боковых панелей';

$_['entry_category_layout_sub']         = 'Категория';
$_['entry_product_columns']             = 'Количество продуктов в каждой строке';
$_['entry_category_style']              = 'Презентация по умолчанию';
$_['entry_category_style_help']         = 'Это может быть изменено пользователем.';

$_['entry_subcategory']                 = 'Подкатегории';
$_['entry_subcategory_show']            = 'Показать <em>Уточнить поиск</em>';
$_['entry_subcategory_thumb_show']      = 'Показать эскизы';
$_['entry_subcategory_thumb_size']      = 'Размер эскиза';


$_['entry_cart_button_sub_help']        = 'По умолчанию они отображаются только на курсоре мыши. Это также влияет на все модули, отображающие продукты.';
$_['entry_display_cart_button']         = '<em>Добавить в корзину</em> кнопка всегда видна:';
$_['entry_display_cart_button_label']   = '<em>Всегда</em> отображать кнопку Добавить в корзину.';

$_['text_extension']                    = 'Extensions';
$_['mobile_menu_title']                 = 'Меню';

// Backend Product Options
$_['tab_feature']                = 'Функция SO';
