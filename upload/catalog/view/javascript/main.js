$(document).ready(function(){
    var width = $(window).width(); // Получаем ширину окна
    //Fullpage slider
    // if (width >= 1199) { // Если ширина окна больше, либо равна 1199
    $("#fullpage").onepage_scroll({
        animationTime: 1500,
        loop: false,
        keyboard: true
    });
    // }

    $('.too-bottom').on('click',function () {
        $("#fullpage").moveTo(4);
    });


    // Declare parallax on layers
    if(width >= 1199){
        jQuery('.parallax-layer').parallax({
            mouseport: jQuery("#parallax-viewport"),
            xparallax: '0.1',
            yparallax: '0.1'
        });
    }

    //Slider products in content
    $('.slider-product').slick({
        centerMode: true,
        centerPadding: '2px',
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        focusOnSelect: true,
        speed: 1000,
        responsive: [
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 1,
                    speed: 200,
                    // edgeFriction: 0,
                    // focusOnSelect: false,
                    // centerMode: false
                }
            }
        ]
    });

    function getChar(event) {
        if (event.which == null) {
            if (event.keyCode < 32) return null;
            return String.fromCharCode(event.keyCode) // IE
        }

        if (event.which != 0 && event.charCode != 0) {
            if (event.which < 32) return null;
            return String.fromCharCode(event.which) // остальные
        }

        return null; // специальная клавиша
    }

    //Contact form input number
    document.getElementById('input-tel').onkeypress = function(e) {

        e = e || event;

        if (e.ctrlKey || e.altKey || e.metaKey) return;

        var chr = getChar(e);

        // с null надо осторожно в неравенствах, т.к. например null >= '0' => true!
        // на всякий случай лучше вынести проверку chr == null отдельно
        if (chr == null) return;

        if (chr < '0' || chr > '9') {
            return false;
        }

    }

});
