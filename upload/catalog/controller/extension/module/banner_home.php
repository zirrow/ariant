<?php
class ControllerExtensionModuleBannerHome extends Controller {
	public function index($setting) {
		static $module = 0;

		$this->load->model('design/banner_home');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');

		$data['banners'] = [];

		$manufacturers = $this->model_design_banner_home->getBanner($setting['banner_id']);

		foreach ($manufacturers as $manufacturer){

			$products_array = $this->model_catalog_product->getProducts(['filter_manufacturer_id' => $manufacturer['manufacturer_id']]);

			$products = [];
			foreach ($products_array as $product){

				if ($product['image']) {
					$image = $this->model_tool_image->resize($product['image'], 170, 350);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', 170, 350);
				}

				$products[] = [
					'name' => $product['name'],
					'description' => html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8'),
					'price' => $price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
					'manufacturer' => $product['manufacturer'],
					'image' => $image,
					'attributes' => $this->model_catalog_product->getProductAttributes($product['product_id'])

				];
			}

			$data['banners'][] = [
				'manufacturer_image' => $this->model_tool_image->resize($manufacturer['image'], $setting['width'], $setting['height']),
				'products' => $products
			];

		}

		$data['module'] = $module++;


		return $this->load->view('extension/module/banner_home', $data);
	}
}