<?php
class ControllerCommonHome extends Controller {

	private $error;

	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') ) {
			if($this->validate()){

				$mail = new Mail($this->config->get('config_mail_engine'));
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
				$mail->smtp_username = $this->config->get('config_mail_smtp_username');
				$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
				$mail->smtp_port = $this->config->get('config_mail_smtp_port');
				$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

				$mail->setTo($this->config->get('config_email'));
				$mail->setFrom($this->config->get('config_email'));
				$mail->setReplyTo($this->request->post['email']);
				$mail->setSender(html_entity_decode($this->request->post['name'], ENT_QUOTES, 'UTF-8'));
				$mail->setSubject('Запрос из формы на сайте');
				$mail->setText($this->request->post['message'].' Номер телефона - '.$this->request->post['phone']);
				$result = $mail->send();

				$this->log->write(print_r($result,1));

				$data['success'] = 'true';
			} else {

				$this->session->data['error'] = $this->error;

				$this->response->redirect($this->url->link('common/home#section4'));
			}
		}


		if (isset($this->request->get['route'])) {
			$this->document->addLink($this->config->get('config_url'), 'canonical');
		}

		$data['title'] = $this->document->getTitle();
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_bg'))) {
			$data['bg'] = $server . 'image/' . $this->config->get('config_bg');
		} else {
			$data['bg'] = '';
		}

		$data['action'] = $this->url->link('common/home', '', true);
		$data['telephone'] = $this->config->get('config_telephone');
		$data['mail'] = $this->config->get('config_email');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if(isset($this->session->data['error']) && !empty($this->session->data['error'])){
			$data['error'] = $this->session->data['error'];
		}

		$this->response->setOutput($this->load->view('common/home', $data));
	}

	protected function validate() {

		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 32)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		if (!filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if (empty($this->request->post['phone'])) {
			$this->error['phone'] = $this->language->get('error_phone');
		}

		return !$this->error;
	}
}
