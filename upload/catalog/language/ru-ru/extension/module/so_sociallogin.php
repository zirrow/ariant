<?php
// Heading
//$_['heading_title'] = 'Вход';
$_['text_colregister'] = '<h2>НОВЕНЬКИЙ ТУТ?</h2>
                            <p class="note-reg">Регистрация простая и бесплатная!</p>
                            <ul class="list-log">
                                <li>Быстрая проверка</li>
                                <li>Сохранить несколько адресов доставки</li>
                                <li>Просмотр, отслеживание заказов и т.д. </li>
                            </ul>';
$_['text_create_account']	= 'Создать аккаунт';
$_['text_forgot_password']	= 'Забыли пароль?';
$_['text_title_popuplogin']	= 'Войти или Зарегистрироваться';
$_['text_title_login_with_social']	= 'Войти в свой аккаунт';