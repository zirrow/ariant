<?php
// Calculator
$_['text_checkout_title']      = 'Оплата в рассрочку';
$_['text_choose_plan']         = 'Выберите свой план';
$_['text_choose_deposit']      = 'Выберите депозит';
$_['text_monthly_payments']    = 'Ежемесячные платежи';
$_['text_months']              = 'Месяцы';
$_['text_term']                = 'Срок';
$_['text_deposit']             = 'Депозит';
$_['text_credit_amount']       = 'Сумма кредита';
$_['text_amount_payable']      = 'Общая сумма к оплате';
$_['text_total_interest']      = 'Общий процентный годовой доход';
$_['text_monthly_installment'] = 'Еемесячный взнос';
$_['text_redirection']         = 'Вы будете перенаправлены на Divido для завершения этого финансового приложения, когда Вы подтвердите свой заказ';