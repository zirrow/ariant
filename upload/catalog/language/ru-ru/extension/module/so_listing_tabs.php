<?php
// Heading
$_['heading_title'] = 'Список вкладок';

// Text
$_['text_tax']      	= 'Без НДС:';
$_['value_price'] 		= 'Цена';
$_['value_name'] 		= 'Название';
$_['value_model'] 		= 'Модель';
$_['value_all'] 		= 'Все';
$_['value_quantity'] 	= 'Количество';
$_['value_rating'] 		= 'Самые рейтинговые';
$_['value_sort_add'] 	= 'Sort Add';
$_['value_date_add'] 	= 'Новые поступления';
$_['value_sell'] 		= 'Хиты продаж';
$_['text_noproduct']    = 'Нет товаров!';
$_['text_sale']      	= 'Распродажа';
$_['text_new']      	= 'Новое';
$_['all_ready']      	= 'Все готово';
$_['load_more']      	= 'Загрузить больше';
$_['button_cart']      	= 'Добавить в корзину';
$_['text_reviews']      = '(%s)';
$_['text_view_all']     = 'Смотреть все';
$_['text_available']    = 'Доступно:';
$_['text_sold']      	= 'Продано:';