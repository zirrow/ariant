<?php
// Heading
$_['heading_title'] 								= 'So Onepage Checkout';
$_['text_checkout_create_account_login']           	= 'Создать учетную запись или войти в систему';
$_['text_confirm_order']           					= 'Подтвердить заказ';
$_['text_shipping_address']           				= 'Адрес доставки';
$_['text_title_shipping_method']           			= 'Способ доставки';
$_['text_title_payment_method']           			= 'Способ оплаты';
$_['text_coupon_voucher']           				= 'У Вас есть купон или ваучер?';
$_['text_enter_coupon_code']           				= 'Введите код купона';
$_['text_enter_voucher_code']           			= 'Введите код ваучера';
$_['text_apply_voucher']           					= 'Применить ваучер';
$_['text_enter_reward_points']           			= 'Введите бонусные баллы';
$_['text_apply_points']           					= 'Применить баллы';
$_['text_shopping_cart']           					= 'Корзина';
$_['text_payment_detail']          					= 'Детали оплаты';
$_['entry_fax']          							= 'Факс';
$_['billing_address']          						= 'Адрес плательщика';
$_['delivery_address']          					= 'Адрес доставки';
//Error
$_['error_comment']									= 'Вы должны добавить комментарии к своему заказу';