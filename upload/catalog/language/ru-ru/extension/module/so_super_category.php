<?php
// Heading
$_['heading_title'] = 'Супер категория';

// Text
$_['text_tax']      = 'Без НДС:';
$_['text_date']      = 'Дата: ';
// So super category
$_['all_ready_label'] 	= 'Все готово';
$_['load_more_label'] 	= 'Загрузить больше';
$_['value_price'] 		= 'Цена';
$_['value_name'] 		= 'Название';
$_['value_model'] 		= 'Модель';
$_['value_quantity'] 	= 'Количество';
$_['value_rating'] 		= 'Рекомендуемые';
$_['value_sort_add'] 	= 'Добавить сортировку';
$_['value_date_add'] 	= 'Новые поступления';
$_['value_sale'] 		= 'Хиты продаж';
$_['text_noitem']      = 'Нет товаров!';
$_['text_sale']      	= 'Распродажа';
$_['text_new']      	= 'Новое';
$_['button_cart']      	= 'Добавить в корзину';
$_['text_view_all']      	= 'Смотреть все';