<?php

// Heading 
$_['heading_title']      	= 'Simple Blog';

$_['text_date_format']		= '<b>d</b> M';

$_['text_popular_all']		= 'Популярные статьи';
$_['text_latest_all']		= 'Недавние посты';

$_['text_no_result']		= 'Безрезультатно!';
$_['text_comments']		= '&nbsp;комментарии';
$_['text_comment']		= '&nbsp;комментарии';
$_['show_all_text']		= 'Смотреть все';
