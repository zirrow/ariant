<?php


// Quickview
$_['button_detail'] 		  		= 'Детали';
$_['text_overview']                 = 'Краткое описание';
$_['text_category_stock_quantity']  = 'Поспешите! Остались только товар(ы) %s!';

// Home page
$_['text_more'] 			  		= 'Больше';
$_['text_shop']  			  		= 'Совершить покупку сейчас';
$_['text_shop_cart']		 		= 'Мой корзина';
$_['text_items_miss']        		= '%s';
$_['text_items']              		= '<span class="number">%s</span> <span class="text">товар(ы)</span> - <span class="number_2">%s</span>';
$_['text_contact']      	  		= 'Контакты';
$_['text_menu']  		 	  		= 'Меню';
$_['text_item_1']             		= 'В Вашей корзине ';
$_['text_item_2']             		= '';
$_['text_item_3']            		 = ' товар(ы)';

$_['text_backtotop']          		= ' вверх ';
$_['text_sub']                		= ' ВСЕ НОВОСТИ НАШЕЙ HURAMA	';
$_['text_stock']              		= ' Акции ';
$_['text_stock1']             		= ' Акции1 ';
$_['text_price_old']          		= ' Старая цена ';
$_['text_price_save']        	 	= ' Экономия ';
$_['text_price_sale']         		= ' Цена продажи ';
$_['text_viewdeals']          		= ' Просмотреть детали ';
$_['text_instock']            		= ' В наличии ';
$_['text_show_cate']          		= ' Показать все категории ';
$_['button_quickview']        		= 'Добавить в быстрый просмотр';
$_['text_hotline']       	  		= 'Горячая линия';
$_['text_compare']       	  		= 'Сравнить';
$_['text_sidebar']       	  		= 'Боковая панель';
$_['text_gridview']       	  		= 'Вид сетки:';

$_['text_neo1']       = 'Трендовые лейблы';
$_['text_neo2']       = 'Flash-продажи';
$_['text_neo3']       = 'Мода';
$_['text_neo4']       = 'Техника';
$_['text_neo5']       = 'Мебель';


// Category page
$_['text_refine_more'] 				  		= 'Больше';
$_['text_refine_less'] 				  		= 'Меньше';
$_['text_product_orders'] 				  	= 'Заказы (%s)';
$_['text_product_orders_detail'] 				  	= '<span>Заказы </span> <span class="number"> (%s)  </span>';
$_['text_reviews']             = '(%s)';

$_['text_viewdeals']  		  		= 'Посмотреть все сделки';
$_['text_new'] 				  		= 'Новое';
$_['text_sale'] 				  	= 'Распродажа';
$_['text_quickview'] 				= 'Быстрый просмотр';
$_['text_labelDay'] 				= 'Дней%!d';
$_['text_labelHour'] 				= 'Часа(ов)%!H';
$_['text_labelMin'] 				= 'Минут';
$_['text_labelSec'] 				= 'Секунд';

$_['text_upsell'] 				  	= 'ТОВАРЫ UPSELL';

// Product page
$_['text_sharethis']  		  = 'Поделиться этим';

$_['text_sidebar']        = 'Боковая панель';
$_['text_size_chart']        = 'Таблица размеров';
$_['tab_video']        = 'Видео';
$_['tab_shipping']        = 'Способ доставки';
$_['text_product_specifics']        = 'Подробности о товаре';
$_['text_product_description']        = 'Описание товара';
$_['text_or']        = 'или';