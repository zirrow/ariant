<?php
// Heading
$_['heading_title']         = '��������� ����� SagePay';

// Text
$_['text_empty']		    = '� ��� ��� ����������� ����';
$_['text_account']          = '����';
$_['text_card']			    = '��������� ������ SagePay';
$_['text_fail_card']	    = '�������� �������� � ��������� ����� ����� SagePay. ���������� � �������������� �������� �� �������.';
$_['text_success_card']     = '����� SagePay ������� �������';
$_['text_success_add_card'] = '����� SagePay ������� ���������';

// Column
$_['column_type']		    = '��� �����';
$_['column_digits']	        = '��������� �����';
$_['column_expiry']	     	= '���� ��������';

// Entry
$_['entry_cc_owner']        = '�������� �����';
$_['entry_cc_type']         = '��� �����';
$_['entry_cc_number']       = '����� �����';
$_['entry_cc_expire_date']  = '���� ��������� ����� �������� �����';
$_['entry_cc_cvv2']         = '�������� ��� ����� (CVV2)';
$_['entry_cc_choice']       = '�������� ������������ �����';

// Button
$_['button_add_card']       = '�������� �����';
$_['button_new_card']       = '�������� ����� �����';



